FROM ubuntu:20.04
RUN apt-get update && apt-get -y upgrade
RUN apt-get -y install curl && apt-get -y install sudo && apt-get -y install systemctl && apt-get -y install systemd
RUN apt-get -y install nano && apt-get -y install vim
RUN bash -c "$(curl -L https://gitlab.com/kimycai/Xray-install/-/raw/main/install-release.sh)" @ install
